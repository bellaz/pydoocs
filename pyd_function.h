#ifndef PYD_FUNCTION_H
#define PYD_FUNCTION_H

#include <Python.h>
// pydoocs function functions provided by the Python bindings
PyObject* pydoocsFunction(PyObject*, PyObject* args);

#endif
