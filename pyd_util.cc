#include "pyd_util.h"
#include <Python.h>
#include "eq_client.h"

#define NO_IMPORT_ARRAY
#define PY_ARRAY_UNIQUE_SYMBOL pydoocs_ARRAY_API
#define NPY_NO_DEPRECATED_API NPY_1_9_API_VERSION
#include <numpy/arrayobject.h>

// Helper function for setting the python exception message from the DOOCS error
void apiSetError(EqData* dataFromDoocs) {
    // retrieve the error code
    int errorCode;
    errorCode = dataFromDoocs->error();
    // retrieve the error message
    char errorMessage[STRING_LENGTH];  // STRING_LENGTH (80) is a macro
    dataFromDoocs->get_string(errorMessage, sizeof(errorMessage));
    // format the output error message
    PyErr_SetObject(PyExc_OSError, Py_BuildValue("{siss}",
                "code", errorCode, "message", errorMessage));
}

// Helper function to call default error
PyObject* defaultError()
{
	int errorCode = 999;
	char errorMessage[] = "data type not supported yet";
	PyErr_SetObject(PyExc_OSError, Py_BuildValue("{siss}",
	    "code", errorCode, "message", errorMessage));
	return NULL;
}

// Helper functions for checking image data and generating an actual image
bool isImageDataValid(IMH* imageHeader)
{
	int imageDataFormat = imageHeader->image_format;
	int imageWidthInPixels = imageHeader->aoi_width;
	int imageHeightInPixels = imageHeader->aoi_height;
	int imageBytesPerImage = imageHeader->length;
	int imageBytePerPixel = imageHeader->bpp;

	if (imageDataFormat == -1) return false;
	if (imageWidthInPixels == 0 ||imageHeightInPixels == 0 || imageBytesPerImage == 0) return false;
	if (imageBytePerPixel != 1 && imageBytePerPixel != 2) return false;

	return true;
}


template <typename T>
void fillImageWithNumpy(u_char** imageValue, PyObject** imageArray, int imageHeightInPixels, int imageWidthInPixels) {
    T** array = new T*[imageHeightInPixels];
    for (int y = 0; y < imageHeightInPixels; ++y)
        array[y] = new T[imageWidthInPixels];

    for (int y = 0; y < imageHeightInPixels; ++y) {
        for (int x = 0; x < imageWidthInPixels; ++x) {
            int pixelIndex = y * imageWidthInPixels + x;
            array[y][x] = (T)* ((T*) *imageValue + pixelIndex);
        }
    }

    T *p = (T *) PyArray_DATA((PyArrayObject*) *imageArray);
    for (int y = 0; y < imageHeightInPixels; ++y) {
        memcpy(p, array[y], sizeof(T) * imageWidthInPixels);
        p += imageWidthInPixels;
    }
    for (int y = 0; y < imageHeightInPixels; ++y)
        delete [] array[y];
    delete [] array;
}

template <typename T>
void fillImageWithoutNumpy(u_char** imageValue, PyObject** imageArray, int imageHeightInPixels, int imageWidthInPixels) {
    T dataValue;
    PyObject* row = NULL;
    for (int y = 0; y < imageHeightInPixels; ++y) {
        row = PyTuple_New(imageWidthInPixels);
        for (int x = 0; x < imageWidthInPixels; ++x) {
            int pixelIndex = y * imageWidthInPixels + x;
            dataValue = (T)* ((T*) *imageValue + pixelIndex);
            PyTuple_SetItem(row, x, Py_BuildValue("i", dataValue));
        }
        PyTuple_SetItem(*imageArray, y, row);
    }
}

bool generateImage(u_char** imageValue, IMH* imageHeader, PyObject** imageArray, bool numpySupport)
{
    int imageDataFormat = imageHeader->image_format;
	int imageBytePerPixel = imageHeader->bpp;
	int imageWidthInPixels = imageHeader->aoi_width;
	int imageHeightInPixels = imageHeader->aoi_height;

    switch(imageDataFormat) {
    case TTF2_IMAGE_FORMAT_GRAY:
        if (imageBytePerPixel == 1) {
            if (numpySupport) {
                const int dim = 2;
                npy_intp size[dim] = {imageHeightInPixels, imageWidthInPixels};
                *imageArray = PyArray_SimpleNew(dim, size, NPY_UINT8);
                fillImageWithNumpy<uint8_t>(imageValue, imageArray, imageHeightInPixels, imageWidthInPixels);
                PyArray_ENABLEFLAGS((PyArrayObject*)imageArray, NPY_ARRAY_OWNDATA);
            } else {
                *imageArray = PyTuple_New(imageHeightInPixels);
                fillImageWithoutNumpy<uint8_t>(imageValue, imageArray, imageHeightInPixels, imageWidthInPixels);
            }
        } else if (imageBytePerPixel == 2) {
            if (numpySupport) {
                const int dim = 2;
                npy_intp size[dim] = {imageHeightInPixels, imageWidthInPixels};
                *imageArray = PyArray_SimpleNew(dim, size, NPY_UINT16);
                fillImageWithNumpy<uint16_t>(imageValue, imageArray, imageHeightInPixels, imageWidthInPixels);
                PyArray_ENABLEFLAGS((PyArrayObject*)imageArray, NPY_ARRAY_OWNDATA);
            } else {
                *imageArray = PyTuple_New(imageHeightInPixels);
                fillImageWithoutNumpy<uint16_t>(imageValue, imageArray, imageHeightInPixels, imageWidthInPixels);
            }
        } else {
            return false;
        }
        break;
    default:
        return false;
        break;
    }
    return true;
}
