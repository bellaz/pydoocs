#ifndef PYD_UTIL_H
#define PYD_UTIL_H

#include <Python.h>
#include "eq_client.h"

// Helper function for setting the python exception message from the DOOCS error
void apiSetError(EqData* dataFromDoocs);

// todo
PyObject* defaultError();

// Helper functions for checking image data and generating an actual image 
bool isImageDataValid(IMH* imageHeader);
bool generateImage(u_char** imageValue, IMH* imageHeader, PyObject** imageArray, bool numpySupport);

#endif
