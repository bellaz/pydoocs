#ifndef PYD_READ_H
#define PYD_READ_H

#include <Python.h>

// pydoocs read functions provided by the Python bindings
PyObject* pydoocsRead(PyObject*, PyObject* args, PyObject* kwargs);

#endif
