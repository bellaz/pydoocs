#include "pyd_read.h"
#include <Python.h>
#include "eq_client.h"
#include "pyd_util.h"

#define NO_IMPORT_ARRAY
#define PY_ARRAY_UNIQUE_SYMBOL pydoocs_ARRAY_API
#define NPY_NO_DEPRECATED_API NPY_1_9_API_VERSION
#include <numpy/arrayobject.h>

// pydoocs read functions provided by the Python bindings
PyObject* pydoocsRead(PyObject* Py_UNUSED(ignored), PyObject* args, PyObject* kwargs) {
    PyObject* dataToPython = NULL;  // data from DOOCS get into this PyObject
    const char* doocsAddress;  // address of the DOOCS channel to read from

    // Parsing and assigning function arguments: just the doocsAddress here
    static const char *kwargsList[] = {"address", "numpy", NULL};
    int tmpNumpySupport = 0;
    if (!(PyArg_ParseTupleAndKeywords(args, kwargs, "s|i", (char **) kwargsList,
                                      &doocsAddress, &tmpNumpySupport))) {
        return NULL;
    }
    bool numpySupport = (tmpNumpySupport == 0) ? false : true;

    EqData dataFromDoocs;  // encapsulates data to be read from DOOCS

    // reading data including error handling
    bool has_error;
    Py_BEGIN_ALLOW_THREADS
    api_init(0.1f);  // setting update rate to 10 Hz
    has_error = api_read(doocsAddress, &dataFromDoocs, API_RETRY);
    Py_END_ALLOW_THREADS
    if (has_error) {
        apiSetError(&dataFromDoocs);  // raising OSError with error message
        return NULL;
    }

    // keys of the returned PyObject (a dict)
    const char * dataString = "data";
    const char * typeString = "type";
    const char * channelString = "channel";
    const char * timestampString = "timestamp";
    const char * macropulseString = "macropulse";

    // generating timestamp in seconds with microsecond resolution
    int timestampSec, timestampMicroSec;
    dataFromDoocs.time(&timestampSec, &timestampMicroSec);
    double timestamp = timestampSec + (double) timestampMicroSec / 1e6;

    // generating macropulse number
    int64_t mpNumber = dataFromDoocs.mpnum();

    // Retrieving explicit data type and returning its content to Python
    switch (dataFromDoocs.type()) {
        case DATA_BOOL: case DATA_INT:  // _BOOL is like _INT with 0 and 1 only
            dataToPython = Py_BuildValue("{sisssssdsl}",
                dataString, dataFromDoocs.get_int(),
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            break;
        case DATA_FLOAT:
            dataToPython = Py_BuildValue("{sfsssssdsl}",
                dataString, dataFromDoocs.get_float(),
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            break;
        case DATA_DOUBLE:
            dataToPython = Py_BuildValue("{sdsssssdsl}",
                dataString, dataFromDoocs.get_double(),
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            break;
        case DATA_STRING:
            char strData[STRING_LENGTH];
            // copy text data from DOOCS to the variable strData
            dataFromDoocs.get_string(strData, sizeof(strData));
            dataToPython = Py_BuildValue("{sssssssdsl}",
                dataString, strData,
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            break;
		case DATA_TEXT:
		{
			std::size_t text_length = dataFromDoocs.array_length();
			char* strTxtData = (char *) malloc((text_length+1) * sizeof(char));
			// copy text data from DOOCS to the variable strTxtData
			dataFromDoocs.get_string(strTxtData, text_length);
			dataToPython = Py_BuildValue("{sssssssdsl}",
			dataString, strTxtData,
			typeString, dataFromDoocs.type_string(),
			channelString, doocsAddress,
			timestampString, timestamp,
			macropulseString, mpNumber);
			if (strTxtData != NULL) free(strTxtData);
			break;
		}
        case DATA_A_INT:
        {// restricting scope to allow variable declaration/initialization here
            int* arrayData = dataFromDoocs.get_int_array();
            int arrayLength = dataFromDoocs.array_length();

            PyObject* arrayTuple = PyTuple_New(arrayLength);
            for (int i = 0; i < arrayLength; ++i) {
                PyTuple_SetItem(arrayTuple, i,
                    Py_BuildValue("i", arrayData[i]));
            }
            // fill the return object of type dict
            dataToPython = Py_BuildValue("{sOsssssdsl}",
                dataString, arrayTuple,
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            Py_DECREF(arrayTuple);
            break;
        }
        case DATA_A_LONG:
        {// restricting scope to allow variable declaration/initialization here
            long long* arrayData = dataFromDoocs.get_long_array();
            int arrayLength = dataFromDoocs.array_length();

            PyObject* arrayTuple = PyTuple_New(arrayLength);
            for (int i = 0; i < arrayLength; ++i) {
                PyTuple_SetItem(arrayTuple, i,
                    Py_BuildValue("l", arrayData[i]));
            }
            // fill the return object of type dict
            dataToPython = Py_BuildValue("{sOsssssdsl}",
                dataString, arrayTuple,
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            Py_DECREF(arrayTuple);
            break;
        }
        case DATA_A_FLOAT:
        {// restricting scope to allow variable declaration/initialization here
            float* arrayData = dataFromDoocs.get_float_array();
            int arrayLength = dataFromDoocs.array_length();

            PyObject* arrayTuple = PyTuple_New(arrayLength);
            for (int i = 0; i < arrayLength; ++i) {
                PyTuple_SetItem(arrayTuple, i,
                    Py_BuildValue("f", arrayData[i]));
            }
            // fill the return object of type dict
            dataToPython = Py_BuildValue("{sOsssssdsl}",
                dataString, arrayTuple,
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            Py_DECREF(arrayTuple);
            break;
        }
        case DATA_A_DOUBLE:
        {// restricting scope to allow variable declaration/initialization here
            double* arrayData = dataFromDoocs.get_double_array();
            int arrayLength = dataFromDoocs.array_length();

            PyObject* arrayTuple = PyTuple_New(arrayLength);
            for (int i = 0; i < arrayLength; ++i) {
                PyTuple_SetItem(arrayTuple, i,
                    Py_BuildValue("d", arrayData[i]));
            }
            // fill the return object of type dict
            dataToPython = Py_BuildValue("{sOsssssdsl}",
                dataString, arrayTuple,
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            Py_DECREF(arrayTuple);
            break;
        }
        case DATA_A_TDS:
        {// restricting scope to allow variable declaration/initialization here
            TDS *history;
            int historyLength = dataFromDoocs.length();

            PyObject* historyTuple = PyTuple_New(historyLength);
            for (int i = 0; i < historyLength; ++i) {
                history = dataFromDoocs.get_tds(i);
                PyTuple_SetItem(historyTuple, i,
                    Py_BuildValue("(ifB)", history->tm, history->data,
                        history->status));
            }
            // fill the return object of type dict
            dataToPython = Py_BuildValue("{sOsssssdsl}",
                dataString, historyTuple,
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            Py_DECREF(historyTuple);
            break;
        }
        case DATA_XYZS: case DATA_A_XYZS:
        {// restricting scope to allow variable declaration/initialization here
            int status;
            float x, y, z;
            char* comment;
            int arrayLength =  dataFromDoocs.length();
            // generate tuple with xyzs data including status
            PyObject* arrayTuple = PyTuple_New(arrayLength);
            for (int i = 0; i < arrayLength; ++i) {
                dataFromDoocs.get_xyzs(&status, &x, &y, &z , &comment, i);
                PyTuple_SetItem(arrayTuple, i,
                    Py_BuildValue("(ifffs)", status, x, y, z, comment));
            }
            // fill the return object of type dict
            dataToPython = Py_BuildValue("{sOsssssdsl}",
                dataString, arrayTuple,
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            Py_DECREF(arrayTuple);
            break;
        }
		case DATA_XY: case DATA_A_XY:
		{
			float x, y;
			int arrayLength =  dataFromDoocs.length();
			// generate tuple with xy data including status
            PyObject* arrayTuple = PyTuple_New(arrayLength);
            for (int i = 0; i < arrayLength; ++i) {
                dataFromDoocs.get_xy(&x, &y, i);
                PyTuple_SetItem(arrayTuple, i,
                    Py_BuildValue("(ff)", x, y));
            }
            // fill the return object of type dict
            dataToPython = Py_BuildValue("{sOsssssdsl}",
                dataString, arrayTuple,
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            Py_DECREF(arrayTuple);
			break;
		}
		case DATA_USTR:
        {
            int i1;
            // when used as part of the *-op wildcard, f2 is not used
            float f1, f2;
            time_t tm;
            char comment[STRING_LENGTH];
            char* commentData;

            dataFromDoocs.get_ustr(&i1, &f1, &f2, &tm, &commentData, 0);

            PyObject* output = Py_BuildValue("{sisfsfslss}","int1", i1, "float1", f1, "float2",
                    f2, "int2", tm, "str", comment);

            dataToPython = Py_BuildValue("{sOsssssdsl}",
                dataString, output,
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            Py_DECREF(output);
            break;
        }
        case DATA_A_USTR:
        {
            int i1;
            // when used as part of the *-op wildcard, f2 is not used
            float f1, f2;
            time_t tm;
            char comment[STRING_LENGTH];
            char* commentData;

            int arrayLength =  dataFromDoocs.length();
            // generate tuple with its data
            PyObject* output;
            PyObject* arrayTuple = PyTuple_New(arrayLength);
            for (int i = 0; i < arrayLength; ++i) {
                dataFromDoocs.get_ustr(&i1, &f1, &f2, &tm, &commentData, i);
                if (commentData) strncpy(comment, commentData, sizeof(comment));
                PyTuple_SetItem(arrayTuple, i, Py_BuildValue("{sisfsfslss}","int1", i1, "float1", f1, "float2",
                    f2, "int2", tm, "str", comment));
            }
            output = arrayTuple;
            // fill the return object of type dict
            dataToPython = Py_BuildValue("{sOsssssdsl}",
                dataString, output,
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            Py_DECREF(output);
            break;
        }
        case DATA_IFFF:
        {
        	int i1;
        	float f1, f2, f3;

        	dataFromDoocs.get_ifff(&i1, &f1, &f2, &f3);

        	PyObject* output = Py_BuildValue("{sisfsfsf}", "int", i1,
        									 "float1", f1, "float2", f2,
											 "float3", f3);

        	// fill the return object of type dict
        	dataToPython = Py_BuildValue("{sOsssssdsl}",
        		dataString, output,
        	    typeString, dataFromDoocs.type_string(),
        	    channelString, doocsAddress,
        	    timestampString, timestamp,
        	    macropulseString, mpNumber);
            Py_DECREF(output);
            break;
        }
        case DATA_SPECTRUM: case DATA_GSPECTRUM:
        {// restricting scope to allow variable declaration/initialization here
            float* spectrumData;
            float spectrumStart, spectrumIncrement;
            // the following variables only needed to match get_spectrum(),
            // otherwise they are not used yet at all
            char* dummyComment;
            int dummyInt;
            u_int status;
            time_t dummyTimeObj;
            // get type of spectrum and assign value to spectrumStart,
            // spectrumIncrement, and spectrumData
            int spectrumType = dataFromDoocs.get_spectrum(&dummyComment,
                &dummyInt, &dummyTimeObj, &spectrumStart, &spectrumIncrement,
                &status, &spectrumData, &dummyInt);
            //spectrumType can be 1 (_SPECTRUM), 2 (_GSPECTRUM), or 0 otherwise
            if (!(spectrumType == 1 || spectrumType == 2)) {
                PyErr_SetString(PyExc_OSError, "wrong spectrum data type");
                return NULL;
            }
            // create and fill array-like container for returning to python
            int spectrumLength = dataFromDoocs.length();
            PyObject* spectrumTuple = PyTuple_New(spectrumLength);
            for (int i = 0; i < spectrumLength; ++i) {
                PyTuple_SetItem(spectrumTuple, i, Py_BuildValue("(ff)",
                    spectrumStart + i * spectrumIncrement, spectrumData[i]));
            }
            // fill the return object of type dict
            const char * statusString = "status";
            dataToPython = Py_BuildValue("{sOsssssdslsi}",
                dataString, spectrumTuple,
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber,
                statusString, status);
            Py_DECREF(spectrumTuple);
            break;
        }
        case DATA_IMAGE:
        {
        	IMH imageHeader;
        	u_char* imageValues;
        	int dummy;

        	int valid = dataFromDoocs.get_image(&imageValues, &dummy, &imageHeader);
        	if (valid == 0) {
                PyErr_SetString(PyExc_OSError, "image data type invalid");
                return NULL;
            }

        	PyObject* imageArray = NULL;

            if (isImageDataValid(&imageHeader)) {
        		bool isImageGenerationValid = generateImage(&imageValues, &imageHeader, &imageArray, numpySupport);
        		if (!isImageGenerationValid) return defaultError();
        	} else {
        		PyErr_SetString(PyExc_OSError, "image header invalid");
        		return NULL;
        	}


        	time_t timestampSec, timestampMicroSec;
        	dataFromDoocs.get_timestamp(&timestampSec, &timestampMicroSec, &dummy);
        	timestamp = timestampSec + (double) timestampMicroSec / 1e6;

			// macropulse number is given by the image's event number
			mpNumber = imageHeader.event;
			
        	// fill the return object of type dict
        	dataToPython = Py_BuildValue("{sOsssssdsl}",
        		dataString, imageArray,
				typeString, dataFromDoocs.type_string(),
        	    channelString, doocsAddress,
        	    timestampString, timestamp,
        	    macropulseString, mpNumber);
            Py_DECREF(imageArray);
            break;
        }
        case DATA_A_TS_FLOAT:
        {
            //get_ts_float (u_int *tsp, u_short *tmsp, u_short *sp, float *vp, int index = 0)
            float history;
            int historyLength = dataFromDoocs.length();

            u_int seconds;
            u_short milliseconds;
            u_short status;
            double time;

            PyObject* historyTuple = PyTuple_New(historyLength);
            for (int i = 0; i < historyLength; ++i) {
                dataFromDoocs.get_ts_float(&seconds, &milliseconds, &status, &history, i);
                time = seconds + ((double) milliseconds) /1000;

                PyTuple_SetItem(historyTuple, i,
                    Py_BuildValue("(ffi)", time, history, status));
            }
            // fill the return object of type dict
            dataToPython = Py_BuildValue("{sOsssssdsl}",
                dataString, historyTuple,
                typeString, dataFromDoocs.type_string(),
                channelString, doocsAddress,
                timestampString, timestamp,
                macropulseString, mpNumber);
            Py_DECREF(historyTuple);
            break;
        }
        default:  // raising OSError with error message
            return defaultError();
    }
    Py_BEGIN_ALLOW_THREADS
    api_reset(doocsAddress);
    Py_END_ALLOW_THREADS
    return dataToPython;
}
