#ifndef PYD_NAMES_H
#define PYD_NAMES_H

#include <Python.h>
// pydoocs names functions provided by the Python bindings
PyObject* pydoocsNames(PyObject*, PyObject* args);

#endif
