#include "pyd_function.h"
#include <Python.h>
#include "eq_client.h"
#include "pyd_util.h"

// pydoocs read functions provided by the Python bindings
PyObject* pydoocsFunction(PyObject* Py_UNUSED(ignored), PyObject* args) {
    PyObject* dataFromPython = NULL;  // data to DOOCS get into this PyObject
    const char* doocsAddress;  // address of the DOOCS channel to read from

    // Parsing and assigning function arguments: doocsAddress and data to write
    if (!PyArg_ParseTuple(args, "sO", &doocsAddress, &dataFromPython)) {
        return NULL;
    }

    api_init(0.1f);  // setting update rate to 10 Hz

    // Converting input data (PyObject) to a utf-8 encoded string
    dataFromPython = PyObject_Str(dataFromPython);
    dataFromPython = PyUnicode_AsEncodedString(dataFromPython, "utf-8", "err");
    const char* dataFromPythonString = PyBytes_AS_STRING(dataFromPython);

    EqData dataToDoocs;  // encapsulates data to be written to DOOCS
    EqData dataFromDoocs;  // encapsulates data to be read from DOOCS
    // pre-reading channel in order to get access to the method set_from_string,
    // and finally writing the data including error handling
    bool read_has_error, set_has_no_error, write_has_error;
    read_has_error = api_read(doocsAddress, &dataToDoocs, API_RETRY);
    set_has_no_error = dataToDoocs.set_from_string(dataFromPythonString);
    write_has_error = api_write(doocsAddress, &dataToDoocs, &dataFromDoocs, API_RETRY);
    if (read_has_error || !set_has_no_error || write_has_error)
    {
        apiSetError(&dataToDoocs);
        return NULL;
    }

    PyObject* dataToPython = NULL;  // data from DOOCS get into this PyObject

    // keys of the returned PyObject (a dict)
    const char * dataString = "data";
    const char * typeString = "type";
    const char * channelString = "channel";
    const char * timestampString = "timestamp";
    const char * macropulseString = "macropulse";

     // generating timestamp in seconds with microsecond resolution
    int timestampSec, timestampMicroSec;
    dataFromDoocs.time(&timestampSec, &timestampMicroSec);
    double timestamp = timestampSec + (double) timestampMicroSec / 1e6;

    // generating macropulse number
    int64_t mpNumber = dataFromDoocs.mpnum();

    dataToPython = Py_BuildValue("{sfsssssdsl}",
        dataString, dataFromDoocs.get_float(),
        typeString, dataFromDoocs.type_string(),
        channelString, doocsAddress,
        timestampString, timestamp,
        macropulseString, mpNumber);

    api_reset(doocsAddress);
    return dataToPython;
}
