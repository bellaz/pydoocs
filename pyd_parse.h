/*
 * pyd_parse.h provides functions to parse python objects in C++ types
 *
 *  Author: Andrea Bellandi andrea.bellandi@desy.de
 *
 */ 

#ifndef PYD_PARSE
#define PYD_PARSE

#include <Python.h>
#include <sys/types.h>
#include <sstream>
#include <string>
#include <vector>
#include <tuple>

bool parsePyBool(PyObject* py_obj, bool& parsed, std::string& error_str);
   /*
    * Parse a bool from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to short that describe errors in parsing
    */

bool parsePyUChar(PyObject* py_obj, u_char& parsed, std::string& error_str);
   /*
    * Parse a u_char from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to short that describe errors in parsing
    */

bool parsePyShort(PyObject* py_obj, short& parsed, std::string& error_str);
   /*
    * Parse a short from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to short that describe errors in parsing
    */

bool parsePyUShort(PyObject* py_obj, u_short& parsed, std::string& error_str);
   /*
    * Parse a u_short from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to short that describe errors in parsing
    */

bool parsePyInt(PyObject* py_obj, int& parsed, std::string& error_str);
   /*
    * Parse a int from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to int that describe errors in parsing
    */
 
bool parsePyUInt(PyObject* py_obj, u_int& parsed, std::string& error_str);
   /*
    * Parse a int from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to int that describe errors in parsing
    */

bool parsePyTimeT(PyObject* py_obj, time_t& parsed, std::string& error_str);
   /*
    * Parse a time_t from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to time_t that describe errors in parsing
    */
 
bool parsePyLong(PyObject* py_obj, long& parsed, std::string& error_str);
   /*
    * Parse a long from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to int that describe errors in parsing
    */
   
bool parsePyULong(PyObject* py_obj, u_long& parsed, std::string& error_str);
   /*
    * Parse a u_long from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to int that describe errors in parsing
    */

bool parsePyLongLong(PyObject* py_obj, long long& parsed, std::string& error_str);
   /*
    * Parse a long long from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to std::string that describe errors in parsing
    */


bool parsePyFloat(PyObject* py_obj, float& parsed, std::string& error_str);
   /*
    * Parse a float from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to float that describe errors in parsing
    */
 
bool parsePyDouble(PyObject* py_obj, double& parsed, std::string& error_str);
   /*
    * Parse a double from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to std::string that describe errors in parsing
    */


bool parsePyChar(PyObject* py_obj, char& parsed, std::string& error_str);
   /*
    * Parse a char from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to std::string that describe errors in parsing
    */

bool parsePyString(PyObject* py_obj, std::string& parsed, std::string& error_str);
   /*
    * Parse a std::string from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to std::string that describe errors in parsing
    */

bool parsePyType(PyObject* py_obj, bool& parsed, std::string& error_str);
    /*
     *  Alias for parsePyBool
     */

bool parsePyType(PyObject* py_obj, u_char& parsed, std::string& error_str);
    /*
     *  Alias for parsePyUChar
     */

bool parsePyType(PyObject* py_obj, short& parsed, std::string& error_str);
    /*
     *  Alias for parsePyShort
     */

bool parsePyType(PyObject* py_obj, u_short& parsed, std::string& error_str);
    /*
     *  Alias for parsePyUShort
     */

bool parsePyType(PyObject* py_obj, int& parsed, std::string& error_str);
    /*
     *  Alias for parsePyInt
     */

bool parsePyType(PyObject* py_obj, u_int& parsed, std::string& error_str);
    /*
     *  Alias for parsePyUInt
     */

bool parsePyType(PyObject* py_obj, time_t& parsed, std::string& error_str);
    /*
     *  Alias for parsePyTimeT
     */

bool parsePyType(PyObject* py_obj, long& parsed, std::string& error_str);
    /*
     *  Alias for parsePyLong
     */

bool parsePyType(PyObject* py_obj, u_long& parsed, std::string& error_str);
    /*
     *  Alias for parsePyULong
     */

bool parsePyType(PyObject* py_obj, long long& parsed, std::string& error_str);
    /*
     *  Alias for parsePyLongLong
     */

bool parsePyType(PyObject* py_obj, float& parsed, std::string& error_str);
    /*
     *  Alias for parsePyFloat
     */
   
bool parsePyType(PyObject* py_obj, double& parsed, std::string& error_str);
    /*
     *  Alias for parsePyDouble
     */

bool parsePyType(PyObject* py_obj, char& parsed, std::string& error_str);
    /*
     *  Alias for parsePyChar
     */

bool parsePyType(PyObject* py_obj, std::string& parsed, std::string& error_str);
    /*
     *  Alias for parsePyString
     */



template<typename ...TupleTypes> 
class tuple_types {
    /*
     * Support private class
     * Parse a Python iterator in a C++ std::tuple
     * py_it: a PyObject iterator
     * tuple_ref: the std::tuple to be filled
     * error_str: the error string
     *
     */

    public:
        // Short explanation: this code use template metaprogramming to generate the code
        // to fill the passed tuple. The function are recursively generated until
        // no tuple field has to be filled
        //

    static bool parsePyIterator(PyObject* py_it, std::tuple<TupleTypes...>& tuple_ref, std::string& error_str){
        
        return parsePyIterator<0, TupleTypes...>(py_it, tuple_ref, error_str);
    }

    private:
    template<unsigned int N> 
    static bool parsePyIterator(PyObject* py_it, std::tuple<TupleTypes...>&, std::string& error_str){
        // this is the last function of the recursion. it checks for additional elements 
        // and returns true if successful
        PyObject* py_el = PyIter_Next(py_it);
        
        if (py_el) {

            Py_DECREF(py_el);
            error_str = "failed to build tuple - too many elements";
            return false;
        }

        return true;
    }

    template<unsigned int N, typename DecodeType, typename ...LeftTypes> 
    static bool parsePyIterator(PyObject* py_it, std::tuple<TupleTypes...>& tuple_ref, std::string& error_str){
        // this is the recursive function. parse the N-th element of the std::tuple and
        // pass the control to itself.
        
        PyObject* py_el = PyIter_Next(py_it);

        if (!py_el) {

            std::ostringstream error_stream;
            error_stream << "tuple like - failed to get element " << N << " too few elements" ;
            error_str = error_stream.str();;

            return false;
        }
        
        if (!parsePyType(py_el, std::get<N>(tuple_ref), error_str)){
            
            std::ostringstream error_stream;
            error_stream << "tuple like - failed to get element " << N << " :" << error_str;
            error_str = error_stream.str();    
        }

        Py_DECREF(py_el);
        return parsePyIterator<N+1, LeftTypes...>(py_it, tuple_ref, error_str);
    }

};

template<typename ...Types> bool parsePyIterable(PyObject* py_obj, std::tuple<Types... >& parsed, std::string& error_str){
   /*
    * Parse a std::tuple from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    & parsed: pointer to a location where the parsed data has to be stored
    * error: reference to int that describe errors in parsing
    */
    PyObject* py_it = PyObject_GetIter(py_obj);

    if (!py_it){

        PyErr_Clear();
        error_str = "tuple like - unable to get an iterator";
        return false;
    }
   
    const bool result = tuple_types<Types... >::parsePyIterator(py_it, parsed, error_str);
    Py_DECREF(py_it);    

    return result;
}

template<typename ...Types> 
bool parsePyType(PyObject* py_obj, std::tuple<Types... >& parsed, std::string& error_str){
    /*
     *  Alias for parsePyIterable
     */

    return parsePyIterable<Types... >(py_obj, parsed, error_str);
}

template<typename AType>
bool parsePyArray(PyObject* py_obj, std::vector<AType>& parsed, std::string& error_str){
   /*
    * Parse a sequence from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    & parsed: pointer to a location where the parsed data has to be stored
    * error: reference to int that describe errors in parsing
    */
   
    long int array_len = 0;

    if(!PySequence_Check(py_obj)) {

        error_str = "list like - object not a sequence";
        return false;
    }

    array_len = PySequence_Size(py_obj);
    if(array_len == -1){

        error_str = "list like - unable to find length of the object";
        return false;
    }
    
    PyObject* py_it = PyObject_GetIter(py_obj);

    if (!py_it){

        PyErr_Clear();
        error_str = "std::tuple like - unable to get an iterator";
        return false;
    }

    for(long int i = 0; i < array_len; i++){

        PyObject* py_el = PyIter_Next(py_it);
        if (!py_el) {
            
            Py_DECREF(py_it);    
            std::ostringstream error_stream;
            error_stream << "list like - unable to read element " << i << " ";
            error_str = error_stream.str();
            return false;
        }

        AType a_type;
        if (!parsePyType(py_el, a_type, error_str)){

            Py_DECREF(py_el);
            Py_DECREF(py_it);
            std::ostringstream error_stream;
            error_stream  << "list like - unable to parse element " << i << " :" << error_str;
            error_str = error_stream.str();
            return false;
        }

        Py_DECREF(py_el);
        parsed.push_back(a_type);
    }

    Py_DECREF(py_it);
    
    return true;
}

template<typename AType>
bool parsePyType(PyObject* py_obj, std::vector<AType>& parsed, std::string& error_str){
    /*
     *  Alias for parsePyArray
     */

    return parsePyArray<AType>(py_obj, parsed, error_str);
}

#endif
