import os
import sys
import glob
import time
import unittest
import numpy as np

# Set correct DOOCS environment dependent on target architecture
arch = os.environ.get('DOOCSARCH', 'Darwin-x86_64')
path = glob.glob("../../../" + arch + "/obj/library/python/pydoocs")
sys.path.insert(0, path[0])
sys.path.insert(0, path[0])


class TestPydoocs(unittest.TestCase):
    """ Test class for testing the pydoocs module and its functions. """
    pydoocs = None

    @classmethod
    def setUpClass(cls):
        cls._check_import_pydoocs_with_pyqt5()

        # base address for the properties of different data types below
        base_address = "TEST.DOOCS/UNIT_TEST_SUPPORT/PY_DOOCS/"

        cls._check_address_availability(base_address)
        cls._set_addresses(base_address)

        # waiting time time after reading to make sure the new value has been set
        cls._sleep = 0.5

    @classmethod
    def _check_import_pydoocs_with_pyqt5(cls):
        from PyQt5 import QtWidgets
        import pydoocs
        cls.pydoocs = pydoocs

    @classmethod
    def _check_address_availability(cls, base_address):
        cls.pydoocs.names("/".join(base_address.split("/")[:2]) + "/*")

    @classmethod
    def _set_addresses(cls, base_address):
        # addresses for testing data types
        cls.address_bool = base_address + "BOOL"
        cls.address_integer = base_address + "INTEGER"
        cls.address_float = base_address + "FLOAT"
        cls.address_double = base_address + "DOUBLE"
        cls.address_string = base_address + "STRING"
        cls.address_text = base_address + "TEXT"
        cls.address_integer_array = base_address + "INTEGER_ARRAY"
        cls.address_float_array = base_address + "FLOAT_ARRAY"
        cls.address_double_array = base_address + "DOUBLE_ARRAY"
        cls.address_long_array = base_address + "LONG_ARRAY"
        cls.address_xyzs_array = base_address + "XYZS_ARRAY"
        cls.address_xy_array = base_address + "XY_ARRAY"
        cls.address_ustr_array = base_address + "USTR_ARRAY"
        cls.address_ifff_array = base_address + "IFFF_ARRAY"
        cls.address_tds_array = base_address + "TDS_ARRAY"
        cls.address_spectrum = base_address + "SPECTRUM"
        cls.address_gspectrum = base_address + "GSPECTRUM"
        cls.address_image = base_address + "IMAGE"
        cls.address_ts_float_array = "XFEL.DIAG/MPS/DI2409S1.DOSI/SRV.SENSOR_VALUE_03_05.HIST"  # TODO

        # addresses for testing exceptions
        cls.address_except_1 = base_address
        cls.address_except_2 = base_address + "23"
        cls.address_except_3 = base_address + "BYTE_ARRAY"

        # address for testing the * operation (wildcard)
        cls.address_star_operation = "/".join(base_address.split("/")[0:2] + ["*/"]) + "FLOAT"

        # address for testing the names call
        cls.address_names_call = base_address + "NAMES_*"

        # address for testing macropulse number
        cls.address_macropulse = cls.address_bool

        # address for monitor options
        cls.address_monitor_option = base_address + "MONITOR_OPTION"

        # addresses for function/remote procedure calls
        cls.address_square_func_output = base_address + "SQUARE_FUNC_OUTPUT"

    def test_python_version(self):
        self.assertEqual(3, sys.version_info.major)
        self.assertEqual(6, sys.version_info.minor)

    def test_read_function_bool(self):
        """ Testing the read function for functionality for DATA_BOOL. """
        output = self.pydoocs.read(self.address_bool)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], int)  # DOOCS' bool is an int
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_write_function_bool(self):
        """ Testing the write function for functionality for DATA_BOOL. """
        output = self.pydoocs.write(self.address_bool, 1)
        self.assertIsNone(output)

    def test_read_write_correctness_bool(self):
        """ Testing the read/write function for correctness for DATA_BOOL. """
        self.pydoocs.write(self.address_bool, 0)
        time.sleep(self._sleep)
        self.assertEqual(0, self.pydoocs.read(self.address_bool)['data'])
        self.pydoocs.write(self.address_bool, 1)
        time.sleep(self._sleep)
        self.assertEqual(1, self.pydoocs.read(self.address_bool)['data'])

    def test_read_function_int(self):
        """ Testing the read function for functionality for DATA_INT. """
        output = self.pydoocs.read(self.address_integer)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], int)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_write_function_int(self):
        """ Testing the write function for functionality for DATA_INT. """
        output = self.pydoocs.write(self.address_integer, 0)
        self.assertIsNone(output)

    def test_read_write_correctness_int(self):
        """ Testing the read/write function for correctness for DATA_INT. """
        self.pydoocs.write(self.address_integer, 700)
        time.sleep(self._sleep)
        self.assertEqual(700, self.pydoocs.read(self.address_integer)['data'])
        self.pydoocs.write(self.address_integer, 0)
        time.sleep(self._sleep)
        self.assertEqual(0, self.pydoocs.read(self.address_integer)['data'])

    def test_read_function_float(self):
        """ Testing the read function for functionality for DATA_FLOAT. """
        output = self.pydoocs.read(self.address_float)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], float)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_write_function_float(self):
        """ Testing the write function for functionality for DATA_FLOAT. """
        output = self.pydoocs.write(self.address_float, 1.0)
        self.assertIsNone(output)

    def test_read_write_correctness_float(self):
        """ Testing the read/write function for correctness for DATA_FLOAT. """
        self.pydoocs.write(self.address_float, 1.23)
        time.sleep(self._sleep)
        self.assertAlmostEqual(1.23, self.pydoocs.read(self.address_float)['data'])
        self.pydoocs.write(self.address_float, 1.0)
        time.sleep(self._sleep)
        self.assertAlmostEqual(1.0, self.pydoocs.read(self.address_float)['data'])

    def test_read_function_double(self):
        """ Testing the read function for functionality for DATA_DOUBLE. """
        output = self.pydoocs.read(self.address_double)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], float)  # python's float are double
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_write_function_double(self):
        """ Testing the write function for functionality for DATA_DOUBLE. """
        output = self.pydoocs.write(self.address_double, 0.0)
        self.assertIsNone(output)

    def test_read_write_correctness_double(self):
        """ Testing the read/write function for correctness for DATA_DOUBLE. """
        self.pydoocs.write(self.address_double, 1.23)
        time.sleep(self._sleep)
        self.assertAlmostEqual(1.23, self.pydoocs.read(self.address_double)['data'])
        self.pydoocs.write(self.address_double, 0.0)
        time.sleep(self._sleep)
        self.assertAlmostEqual(0.0, self.pydoocs.read(self.address_double)['data'])

    def test_read_function_string(self):
        """ Testing the read function for functionality for DATA_STRING. """
        output = self.pydoocs.read(self.address_string)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], str)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_write_function_string(self):
        """ Testing the write function for functionality for DATA_STRING. """
        output = self.pydoocs.write(self.address_string, "new string")
        self.assertIsNone(output)

    def test_read_write_correctness_string(self):
        """ Testing the read/write function for correctness for DATA_STRING. """
        self.pydoocs.write(self.address_string, "foo")
        time.sleep(self._sleep)
        self.assertEqual("foo", self.pydoocs.read(self.address_string)['data'])
        self.pydoocs.write(self.address_string, "new string")
        time.sleep(self._sleep)
        self.assertEqual("new string", self.pydoocs.read(self.address_string)['data'])

    def test_read_function_Text(self):
        """ Testing the read function for functionality for DATA_TEXT. """
        output = self.pydoocs.read(self.address_text)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], str)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_write_function_Text(self):
        """ Testing the write function for functionality for DATA_TEXT. """
        output = self.pydoocs.write(self.address_text, "test text")
        self.assertIsNone(output)

    def test_read_write_correctness_Text(self):
        """ Testing the read/write function for correctness for DATA_TEXT. """
        self.pydoocs.write(self.address_text, "foo")
        time.sleep(self._sleep)
        self.assertEqual("foo", self.pydoocs.read(self.address_text)['data'])
        self.pydoocs.write(self.address_text, "test text")
        time.sleep(self._sleep)
        self.assertEqual("test text", self.pydoocs.read(self.address_text)['data'])

    def test_read_function_int_array(self):
        """ Testing the read function for functionality for DATA_A_INT. """
        output = self.pydoocs.read(self.address_integer_array)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], int)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_read_function_long_array(self):
        """ Testing the read function for functionality for DATA_A_LONG. """
        output = self.pydoocs.read(self.address_long_array)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], int)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)
        self.assertEqual(output['data'][0], output['macropulse'])

    def test_read_function_xyzs_array(self):
        """ Testing the read function for functionality for DATA_A_XYZS. """
        output = self.pydoocs.read(self.address_xyzs_array)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        self.assertEqual(5, len(output['data'][0]))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], tuple)
        self.assertIsInstance(output['data'][0][0], int)
        self.assertIsInstance(output['data'][0][1], float)
        self.assertIsInstance(output['data'][0][2], float)
        self.assertIsInstance(output['data'][0][3], float)
        self.assertIsInstance(output['data'][0][4], str)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_read_function_xy_array(self):
        """ Testing the read function for functionality for DATA_A_XY. """
        output = self.pydoocs.read(self.address_xy_array)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        self.assertEqual(2, len(output['data'][0]))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], tuple)
        self.assertIsInstance(output['data'][0][0], float)
        self.assertIsInstance(output['data'][0][1], float)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_read_function_ustr_array(self):
        """ Testing the read function for functionality for DATA_A_USTR. """
        output = self.pydoocs.read(self.address_ustr_array)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        self.assertEqual(1, len(output['data']))
        self.assertEqual(5, len(output['data'][0]))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], dict)
        self.assertIsInstance(output['data'][0]['int1'], int)
        self.assertIsInstance(output['data'][0]['float1'], float)
        self.assertIsInstance(output['data'][0]['float2'], float)
        self.assertIsInstance(output['data'][0]['int2'], int)
        self.assertIsInstance(output['data'][0]['str'], str)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_read_function_ifff(self):
        """ Testing the read function for functionality for DATA_IFFF. """
        output = self.pydoocs.read(self.address_ifff_array)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        self.assertEqual(4, len(output['data']))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], dict)
        self.assertIsInstance(output['data']['int'], int)
        self.assertIsInstance(output['data']['float1'], float)
        self.assertIsInstance(output['data']['float2'], float)
        self.assertIsInstance(output['data']['float3'], float)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_read_function_tds_array(self):
        """ Testing the read function for functionality for DATA_A_TDS. """
        output = self.pydoocs.read(self.address_tds_array)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        self.assertEqual(3, len(output['data'][0]))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], tuple)
        self.assertIsInstance(output['data'][0][0], int)
        self.assertIsInstance(output['data'][0][1], float)
        self.assertIsInstance(output['data'][0][2], int)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_read_function_float_array(self):
        """ Testing the read function for functionality for DATA_A_FLOAT. """
        output = self.pydoocs.read(self.address_float_array)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], float)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_read_function_double_array(self):
        """ Testing the read function for functionality for DATA_A_DOUBLE. """
        output = self.pydoocs.read(self.address_double_array)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], float)  # python's double
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_read_function_spectrum(self):
        """ Testing the read function for functionality for DATA_SPECTRUM. """
        output = self.pydoocs.read(self.address_spectrum)
        self.assertIsInstance(output, dict)
        self.assertEqual(6, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIn('status', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], tuple)
        self.assertIsInstance(output['data'][0][0], float)
        self.assertIsInstance(output['data'][0][1], float)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)
        self.assertIsInstance(output['status'], int)
        self.assertEqual(output['type'], "SPECTRUM")
        self.assertEqual(2, len(output['data'][0]))

    def test_read_function_gspectrum(self):
        """ Testing the read function for functionality for DATA_GSPECTRUM. """
        output = self.pydoocs.read(self.address_gspectrum)
        self.assertIsInstance(output, dict)
        self.assertEqual(6, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIn('status', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], tuple)
        self.assertIsInstance(output['data'][0][0], float)
        self.assertIsInstance(output['data'][0][1], float)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)
        self.assertIsInstance(output['status'], int)
        self.assertEqual(output['type'], "GSPECTRUM")
        self.assertEqual(2, len(output['data'][0]))

    def test_read_function_image(self):
        """ Testing the read function for functionality for DATA_IMAGE. """
        output = self.pydoocs.read(self.address_image)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], tuple)
        self.assertIsInstance(output['data'][0][0], int)
        self.assertIsInstance(output['data'][0][1], int)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)
        self.assertTrue(len(output['data']) > 1)
        self.assertTrue(len(output['data'][0]) > 1)
        self.assertTrue(0 != output['macropulse'])

    def test_read_function_image_numpy(self):
        """ Testing the read function for DATA_IMAGE with numpy support """
        output = self.pydoocs.read(self.address_image, numpy=True)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], np.ndarray)
        self.assertIsInstance(output['data'][0], np.ndarray)
        self.assertIsInstance(output['data'][0][0], np.uint8)
        self.assertIsInstance(output['data'][0][1], np.uint8)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)
        self.assertTrue(len(output['data']) > 1)
        self.assertTrue(len(output['data'][0]) > 1)
        self.assertTrue(0 != output['macropulse'])

    def test_read_function_ts_float_array(self):
        """ Testing the read function for DATA_A_TS_FLOAT """
        output = self.pydoocs.read(self.address_ts_float_array)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], tuple)
        self.assertIsInstance(output['data'][0][0], float)
        self.assertIsInstance(output['data'][0][1], float)
        self.assertIsInstance(output['data'][0][2], int)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)
        self.assertTrue(len(output['data']) > 1)
        self.assertTrue(len(output['data'][0]) > 1)

    def test_read_function_star_operation(self):
        """ Testing the read function for functionality for the * operation. """
        output = self.pydoocs.read(self.address_star_operation)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        self.assertEqual(1, len(output['data']))
        self.assertEqual(5, len(output['data'][0]))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], tuple)
        self.assertIsInstance(output['data'][0], dict)
        self.assertIsInstance(output['data'][0]['int1'], int)
        self.assertIsInstance(output['data'][0]['float1'], float)
        self.assertIsInstance(output['data'][0]['float2'], float)
        self.assertIsInstance(output['data'][0]['int2'], int)
        self.assertIsInstance(output['data'][0]['str'], str)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_read_exception(self):
        """ Testing the read function for proper exceptions. """
        with self.assertRaises(OSError) as err:
            self.pydoocs.read(self.address_except_1)
        err_out_1, = err.exception.args
        with self.assertRaises(OSError) as err:
            self.pydoocs.read(self.address_except_2)
        err_out_2, = err.exception.args

        self.assertIsInstance(err_out_1['code'], int)
        self.assertIsInstance(err_out_1['message'], str)
        self.assertIsInstance(err_out_2['code'], int)
        self.assertIsInstance(err_out_2['message'], str)
        self.assertNotEqual(err_out_1['code'], err_out_2['code'])
        self.assertNotEqual(err_out_1['message'], err_out_2['message'])

        with self.assertRaises(OSError) as err:
            self.pydoocs.read(self.address_except_3)
        err_out, = err.exception.args
        self.assertEqual(err_out['code'], 999)
        self.assertEqual(err_out['message'], 'data type not supported yet')

    def test_names_call(self):
        """ Testing the names function for functionality for integer data types. """
        output = self.pydoocs.names(self.address_names_call)
        expected = ("NAMES_1 ", "NAMES_2 ", "NAMES_3 ")
        self.assertIsInstance(output, tuple)
        self.assertIsInstance(output[1], str)
        self.assertEqual(expected, output)

    def test_macropulse_number(self):
        """ Testing the output of a macropulse number. """
        output1 = self.pydoocs.read(self.address_macropulse)
        time.sleep(2)
        output2 = self.pydoocs.read(self.address_macropulse)
        self.assertTrue(output1['macropulse'] != output2['macropulse'] and
                        (output1['macropulse'] != 0 or output2['macropulse'] != 0))

    def test_read_function_square_func_output(self):
        """ Testing the read function for functionality of function/remote procedure call. """
        output = self.pydoocs.read(self.address_square_func_output)
        self.assertIsInstance(output, dict)
        self.assertEqual(5, len(output))
        output_keys = output.keys()
        self.assertIn('data', output_keys)
        self.assertIn('type', output_keys)
        self.assertIn('channel', output_keys)
        self.assertIn('timestamp', output_keys)
        self.assertIn('macropulse', output_keys)
        self.assertIsInstance(output['data'], float)
        self.assertIsInstance(output['type'], str)
        self.assertIsInstance(output['channel'], str)
        self.assertIsInstance(output['timestamp'], float)
        self.assertIsInstance(output['macropulse'], int)

    def test_write_function_square_func_output(self):
        """ Testing the write function for functionality of function/remote procedure call. """
        output = self.pydoocs.write(self.address_square_func_output, 1.0)
        self.assertIsNone(output)

    def test_read_write_correctness_square_func_output(self):
        """ Testing the read/write function for correctness of function/remote procedure call. """
        self.pydoocs.write(self.address_square_func_output, 2.0)
        time.sleep(self._sleep)
        self.assertAlmostEqual(4.0, self.pydoocs.read(self.address_square_func_output)['data'])
        self.pydoocs.write(self.address_square_func_output, 1.0)
        time.sleep(self._sleep)
        self.assertAlmostEqual(1.0, self.pydoocs.read(self.address_square_func_output)['data'])

    # def test_function_rpc_call_square_func_output(self):
    #     self.assertAlmostEqual(16.0, self.pydoocs.function(self.address_square_func_output, 4.0)['data'])
    #     self.assertAlmostEqual(4.0, self.pydoocs.function(self.address_square_func_output, 2.0)['data'])
    #
    # def test_monitor_option(self):
    #     durations = []
    #     for _ in range(10):
    #         durations.append(self._get_duration(self.address_monitor_option))
    #
    #     #print(durations)
    #
    # def _get_duration(self, address):
    #     start = time.time()
    #     ts = self.pydoocs.read(address)["macropulse"]
    #     while ts == self.pydoocs.read(address)["macropulse"]:
    #         pass
    #     return time.time() - start

