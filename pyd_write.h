#ifndef PYD_WRITE_H
#define PYD_WRITE_H

#include <Python.h>
// pydoocs write functions provided by the Python bindings
PyObject* pydoocsWrite(PyObject*, PyObject* args);

#endif
