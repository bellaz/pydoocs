/**
 * Wrapper code for the Python3 bindings to the standard DOOCS client API
 * written in C/C++.
 *
 * author: C. Behrens, O. Hensler
 * version: February 12, 2016
 */

#pragma GCC diagnostic ignored "-Wmissing-field-initializers"

#include <Python.h>  // Python.h must come before all other header files
#include "eq_client.h"
#include "pyd_read.h"
#include "pyd_write.h"
#include "pyd_names.h"
#include <cstdio>
//#include "pyd_function.h"

#define PY_ARRAY_UNIQUE_SYMBOL pydoocs_ARRAY_API
#define NPY_NO_DEPRECATED_API NPY_1_9_API_VERSION
#include <numpy/arrayobject.h>

// Doc strings for the pydoocs module
PyDoc_STRVAR(pydoocs__doc__, "Python3 bindings to the standard DOOCS client "
                             "API written in C/C++.");
// Doc strings for pydoocs' functions
PyDoc_STRVAR(read__doc__, "Read data from the given channel address.");
PyDoc_STRVAR(write__doc__, "Write data to the given channel address.");
PyDoc_STRVAR(names__doc__, "Query names from the given name domain.");
//PyDoc_STRVAR(function__doc__, "Function/remote procedure call with the given channel address.");

// Boilerplate code needed for the Python3 API:
// Listing of the provided DOOCS functions
static PyMethodDef PydoocsMethods[] = {
    {"read", (PyCFunction) pydoocsRead, METH_VARARGS | METH_KEYWORDS, read__doc__},
    {"write", pydoocsWrite, METH_VARARGS, write__doc__},
    {"names", pydoocsNames, METH_VARARGS, names__doc__},
    //{"function", pydoocsFunction, METH_VARARGS, function__doc__},
    {NULL, NULL, 0, NULL} // sentinel
};

// Definition of the Python module
static struct PyModuleDef pydoocsModule = {
    PyModuleDef_HEAD_INIT,
    "pydoocs",
    pydoocs__doc__,
    -1, // set to -1 if no more memory is needed
    PydoocsMethods
};

// Initialization of the Python module
PyMODINIT_FUNC
PyInit_pydoocs(void) {
    import_array();
    return PyModule_Create(&pydoocsModule);
}
