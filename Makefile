#
# File:         Makefile 3
#
# Author:       
#               Copyright 2016
#
#               This program is free software; you can redistribute it
#               and/or  modify it under  the terms of  the GNU General
#               Public  License as  published  by  the  Free  Software
#               Foundation;  either  version 2 of the License, or  (at
#               your option) any later version.
#

DOOCSROOT = ../../..

# to define DOOCSROOT as an absolute path
include $(DOOCSROOT)/$(DOOCSARCH)/DEFINEDOOCSROOT

# to define LIBNO
include ./LIBNO

include $(DOOCSROOT)/$(DOOCSARCH)/CONFIG

OBJDIR = $(DOOCSROOT)/$(DOOCSARCH)/obj/library/python/$(PKGDIR)

SRCDIR = $(DOOCSROOT)/library/python/$(PKGDIR)

# FIXME These two lines needs to move to the platform-dependent CONFIG file
PYTHONPATHBASE = /opt/anaconda4
CPPFLAGS += -I$(PYTHONPATHBASE)/include/python3.6m -std=c++11 -g
CPPFLAGS += -I$(PYTHONPATHBASE)/lib/python3.6/site-packages/numpy/core/include

# Overwrite for now LDFLAGS to not use --no-undefined flag since ABI do not match of Anaconda libs and system libs
LDFLAGS  = -L$(DOOCSLIBS) -L$(SYSDOOCSLIBS) -L$(PYTHONPATHBASE)/lib -lpython3.6m -lstdc++ -g

# Conditional linking for Linux
ifeq ($(DOOCSARCH), Ubuntu-16.04-x86_64)
    LDFLAGS += -Wl,-rpath=/usr/lib/x86_64-linux-gnu/ -lDOOCSapi
endif

LIBRARYOBJ = \
	$(OBJDIR)/pydoocs.o \
	$(OBJDIR)/pyd_names.o \
	$(OBJDIR)/pyd_read.o \
	$(OBJDIR)/pyd_util.o \
	$(OBJDIR)/pyd_write.o\
	$(OBJDIR)/pyd_parse.o


LIBRARYHFILES = \
	$(SRCDIR)/pydoocs.h \
	$(SRCDIR)/pyd_names.h \
	$(SRCDIR)/pyd_read.h \
	$(SRCDIR)/pyd_util.h \
	$(SRCDIR)/pyd_parse.h \
	$(SRCDIR)/pyd_write.h

ALLLIBS = \
	$(OBJDIR)/pydoocs.a \
	$(OBJDIR)/pydoocs.$(EXT)

all:	$(ALLLIBS)
	

$(OBJDIR)/.depend depend:
	@if [ ! -f $(OBJDIR) ] ; then \
		echo ---------- create dir $(OBJDIR) --------------; \
		mkdir -p $(OBJDIR) ; \
	fi
	for i in $(SRCDIR)/*.cc ;do $(CCDEP) $$i ;done > $(OBJDIR)/.depend_temp
	cat $(OBJDIR)/.depend_temp | sed -e "/:/s/^/\$$\(OBJDIR\)\//g" > $(OBJDIR)/.depend
	chmod g+w $(OBJDIR)/.depend*

include $(OBJDIR)/.depend

$(OBJDIR)/pydoocs.a: $(LIBRARYOBJ)
	$(LINK.a) $(LIBRARYOBJ)
	@-ranlib $(OBJDIR)/pydoocs.a
	@-chmod g+w $(OBJDIR)/pydoocs.a
	@echo "----------------$(OBJDIR)/pydoocs.a done---------------"
	@echo

$(OBJDIR)/pydoocs.so.$(LIBNO): $(LIBRARYOBJ) 
	$(LINK.so) $(LIBRARYOBJ) $(LDFLAGS) $(LDLIBS) -lTTFapi
	@-chmod g+w $(OBJDIR)/pydoocs.s*.$(LIBNO)
	@rm -f $(OBJDIR)/pydoocs.so
	@ln -s $(OBJDIR)/pydoocs.so.$(LIBNO) $(OBJDIR)/pydoocs.so
	@echo "------------ $(OBJDIR)/pydoocs.so.$(LIBNO) done ---------------"
	@echo

$(OBJDIR)/pydoocs.$(LIBNO).dylib: $(LIBRARYOBJ)
	$(LINK.dylib) $(LIBRARYOBJ) $(LDFLAGS) $(LDLIBS) -lTTFapi
	@-chmod g+w $(OBJDIR)/pydoocs.$(LIBNO).d*
	@rm -f $(OBJDIR)/pydoocs.dylib
	@ln -s $(OBJDIR)/pydoocs.$(LIBNO).dylib $(OBJDIR)/pydoocs.dylib
	@cp $(OBJDIR)/pydoocs.$(LIBNO).dylib $(OBJDIR)/pydoocs.so
	@echo "------------ $(OBJDIR)/pydoocs.$(LIBNO).dylib done ---------------"
	@echo

localinstall:  $(ALLLIBS)
		# install H files
		@if [ ! -d $(DOOCSINC) ] ; then \
		 echo "- INSTALL: create dir $(DOOCSINC) " ; \
		 mkdir -p $(DOOCSINC) ; \
		 chmod -f g+w $(DOOCSINC) ; \
		fi
		@for i in $(LIBRARYHFILES); do \
		 if [ -f $(DOOCSINC)/`basename $$i` ] ; then \
		    echo "- INSTALL: move $(DOOCSINC)/`basename $$i` to \n  $(DOOCSINC)/`basename $$i`.OLD" ; \
		    mv -f $(DOOCSINC)/`basename $$i` $(DOOCSINC)/`basename $$i`.OLD ; \
		 fi ; \
		 echo "- INSTALL: $$i in \n  $(DOOCSINC)/`basename $$i` " ; \
		 cp $$i $(DOOCSINC) ;\
		 chmod -f g+w $(DOOCSINC)/`basename $$i` ; \
		 done
		# install in the main arch lib dir
		@if [ ! -d $(DOOCSLIBS) ] ; then \
		 echo "- INSTALL: create dir $(DOOCSLIBS) " ; \
		 mkdir -p $(DOOCSLIBS) ; \
		 chmod -f g+w $(DOOCSLIBS) ; \
		fi
		@for p in $(ALLLIBS); do \
		 echo "- INSTALL: $$p in \n  $(DOOCSLIBS)/`basename $$p` " ; \
		 cp $$p $(DOOCSLIBS) ; \
		 chmod -f g+w $(DOOCSLIBS)/`basename $$p` ; \
		 if [ "XSO" = X`echo $$p | awk '/\.so/ {print "SO"} '` ] ; then \
			SONAME=`echo $$p | sed -e "s/\.so.\{0,15\}/.so/g"` ; \
			echo "- INSTALL: ln -s $(DOOCSLIBS)/`basename $$p` $(DOOCSLIBS)/`basename $$SONAME`" ; \
			rm -f $(DOOCSLIBS)/`basename $$SONAME` ; \
			ln -s $(DOOCSLIBS)/`basename $$p` $(DOOCSLIBS)/`basename $$SONAME` ; \
			SAFILE=`echo $$p  | sed -e "s/\.so\./\.a\./g"` ; \
			if [ -f $$SAFILE ] ; then \
			   echo "- INSTALL: $$SAFILE in \n  $(DOOCSLIBS)/`basename $$SAFILE` " ; \
			   cp $$SAFILE $(DOOCSLIBS) ; \
			   chmod -f g+w $(DOOCSLIBS)/`basename $$SAFILE` ; \
			fi ; \
		elif [ "XDYLIB" = X`echo $$p | awk '/\.dylib/ {print "DYLIB"} '` ] ; then \
			DYLIBNAME=`echo $$p | sed -e "s/\.[0-9]\{1,3\}//g"` ; \
			echo "- INSTALL: ln -s $(DOOCSLIBS)/`basename $$p` $(DOOCSLIBS)/`basename $$DYLIBNAME`" ; \
			rm -f $(DOOCSLIBS)/`basename $$DYLIBNAME` ; \
			ln -s $(DOOCSLIBS)/`basename $$p` $(DOOCSLIBS)/`basename $$DYLIBNAME` ; \
			cp $(DOOCSLIBS)/`basename $$p` $(DOOCSLIBS)/`basename $$SONAME` ; \
			SAFILE=`echo $$p | sed -e "s/\(\.[0-9]\{1,2\}\)\{1,3\}\.dylib/.a/g"` ; \
			if [ -f $$SAFILE ] ; then \
				echo "- INSTALL: $$SAFILE in \n  $(DOOCSLIBS)/`basename $$SAFILE` " ; \
				cp $$SAFILE $(DOOCSLIBS) ; \
				chmod -f g+w $(DOOCSLIBS)/`basename $$SAFILE` ; \
			fi ; \
		 fi ; \
		done

developmentinstall:  $(ALLLIBS)
		# install in all INSTLIBDIRSDEV
		-@for i in $(INSTLIBDIRSDEV); do \
		   if [ ! -d $(INSTLIBDIRSDEV) ] ; then \
		      echo "- INSTALL: create dir $(INSTLIBDIRSDEV) " ; \
		      mkdir -p $(INSTLIBDIRSDEV) ; \
		      chmod -f g+w $(INSTLIBDIRSDEV) ; \
		      echo "- INSTALL: create dir $(INSTINCDIRSDEV) " ; \
		      mkdir -p $(INSTINCDIRSDEV) ; \
		      chmod -f g+w $(INSTINCDIRSDEV) ; \
		   fi ; \
		   for k in $(LIBRARYHFILES); do \
		    if [ -f $(INSTINCDIRSDEV)/`basename $$k` ] ; then \
		       echo "- INSTALL: move $(INSTINCDIRSDEV)/`basename $$k` to \n  $(INSTINCDIRSDEV)/`basename $$k`.OLD" ; \
		       mv $(INSTINCDIRSDEV)/`basename $$k` $(INSTINCDIRSDEV)/`basename $$k`.OLD ; \
		    fi ; \
		    echo "- INSTALL: $$k in \n  $(INSTINCDIRSDEV)/`basename $$k` " ; \
		    cp $$k $(INSTINCDIRSDEV) ; \
		    chmod -f g+w $(INSTINCDIRSDEV)/`basename $$k` ; \
		   done ; \
		  for p in $(ALLLIBS); do \
		   if [ "XSO" = X`echo $$p | awk '/\.so/ {print "SO"} '` ] ; then \
			echo "- INSTALL: $$p in \n  $$i/`basename $$p` " ; \
			cp $$p $$i ; \
			chmod -f g+w $$i/`basename $$p` ; \
			SONAME=`echo $$p | sed -e "s/\.so.\{0,15\}/.so/g"` ; \
			echo "- INSTALL: ln -s $$i/`basename $$p` $$i/`basename $$SONAME`" ; \
			rm -f $$i/`basename $$SONAME` ; \
			( cd $(INSTLIBDIRSDEV) ; ln -s `basename $$p` `basename $$SONAME` ) ; \
			SAFILE=`echo $$p  | sed -e "s/\.so.\{0,15\}/\.a/g"` ; \
			echo "$$SAFILE " ; \
			if [ -f $$SAFILE ] ; then \
			   echo "- INSTALL: $$SAFILE in \n  $$i/`basename $$SAFILE` " ; \
			   cp $$SAFILE $$i ; \
			   chmod -f g+w $$i/`basename $$SAFILE` ; \
			fi ; \
		   elif [ "XDYLIB" = X`echo $$p | awk '/\.dylib/ {print "DYLIB"} '` ] ; then \
			echo "- INSTALL: $$p in \n  $$i/`basename $$p` " ; \
			cp $$p $$i ; \
			chmod -f g+w $$i/`basename $$p` ; \
			DYLIBNAME=`echo $$p | sed -e "s/\.[0-9]\{1,3\}//g"` ; \
			echo "- INSTALL: ln -s $$i/`basename $$p` $$i/`basename $$DYLIBNAME`" ; \
			rm -f $$i/`basename $$DYLIBNAME` ; \
			( cd $(INSTLIBDIRSDEV) ; ln -s `basename $$p` `basename $$DYLIBNAME` ) ; \
			SAFILE=`echo $$p | sed -e "s/\(\.[0-9]\{1,2\}\)\{1,3\}\.dylib/.a/g"` ; \
			echo "$$SAFILE " ; \
			if [ -f $$SAFILE ] ; then \
				echo "- INSTALL: $$SAFILE in \n  $$i/`basename $$SAFILE` " ; \
				cp $$SAFILE $$i ; \
				chmod -f g+w $$i/`basename $$SAFILE` ; \
			fi ; \
			SOFILE=`echo $$p | sed -e "s/\(\.[0-9]\{1,2\}\)\{1,3\}\.dylib/.so/g"` ; \
			echo "- INSTALL: $$p as $$i/`basename $$SOFILE` " ; \
			echo cp $$p $$i/`basename $$SOFILE` ; \
			chmod -f g+w $$i/`basename $$SOFILE` ; \
		   fi ; \
		 done \
		done

install: $(ALLLIBS)
	make all ; \
	make developmentinstall 
	echo " installed in: $(INSTLIBDIRSDEV)."
	echo " ! NO LOCAL INSTALL ! "

test:
	/opt/anaconda4/bin/python -m unittest discover -s tests -p testPydoocs.py
	
doxygen:
	echo "Running Doxygen "
	-( cat $(PKGDIR).doxygen | sed "s/PROJECT_NUMBER         =.*/PROJECT_NUMBER         = `cut -f2 -d= LIBNO`/" ) | doxygen -
	-chmod -R g+w /web/tesla/doocs/doocs_libs/$(PKGDIR)/

clean:
	rm -f $(LIBRARYOBJ) \
		$(OBJDIR)/pydoocs.$(EXT)
	rm -rf $(OBJDIR)
