// Driver script for Jenkins CI

// Root path of build, where the Jenkins workspace resides
def rootPath = '/doocs/doocssvr1/nightly_builds/'

// URL of Git base address
def doocsRepoUrl = 'http://doocs-git.desy.de/cgit/'

// List of hosts and targets to be used by Jenkins slaves
def buildHostMap = [
    'doocsdev16':'Ubuntu-16.04-x86_64',
    'flashbkr1':'Darwin-x86_64'
    ]

// List of libraries this repository depends upon, aka upstream dependencies
def libraryDependencyList = [
    'clientlib':'doocs/library/common/clientlib'
]

// Name and Git path of repository to be built in this job
def repoMap = [
    pydoocs:'doocs/library/python/pydoocs'
]

// Default mail recipient
def mailRecipient = [
    'tim.wilksen@desy.de'
    ]

// Pre-stage for checking out the target-specific CONFIG file
for (buildHost in mapToList(buildHostMap)) {
    node (buildHost[0]) {
        // Define base dir in workspace
        def workspace = pwd()
        echo "Workspace path is ${workspace}"

        stage(buildHost[0] + ' Prepare Build ') {
            env.DOOCSARCH = buildHost[1]
            checkout([$class: 'GitSCM', branches: [[name: '*/master']], 
                                        browser: [$class: 'CGit', repoUrl: doocsRepoUrl], 
                                        clean: true,
                                        doGenerateSubmoduleConfigurations: false, 
                                        extensions: [[$class: 'CleanBeforeCheckout'], 
                                                    [$class: 'RelativeTargetDirectory', 
                                                        relativeTargetDir: 'doocs/' + buildHost[1]]], 
                                        submoduleCfg: [], 
                                        userRemoteConfigs: [[url: doocsRepoUrl + 'doocs/' + buildHost[1] + '.git/']]])
        }

        echo "BUILD_NUMBER=${env.BUILD_NUMBER}"
        String buildNumber = "${env.BUILD_NUMBER}"

        // Main stage of compiling and linking the repo code
        for (repo in mapToList(repoMap)) {
            println(repo[1])
            try {
                stage(buildHost[1] + ' Build ' + repo[0]) {
                    echo "Running ${env.BUILD_ID} on ${env.JENKINS_URL}"
                    checkout([$class: 'GitSCM', branches: [[name: '*/master']], 
                            browser: [$class: 'CGit', repoUrl: doocsRepoUrl], 
                            doGenerateSubmoduleConfigurations: false,   
                            extensions: [[$class: 'CleanBeforeCheckout'], 
                                         [$class: 'RelativeTargetDirectory', relativeTargetDir: repo[1]],
                                         [$class: 'DisableRemotePoll'],
                                         [$class: 'PathRestriction', excludedRegions: '',includedRegions: repo[1] + '/.*']], 
                            submoduleCfg: [], 
                            userRemoteConfigs: [[url: doocsRepoUrl + repo[1] + '.git/']]])
                    dir(repo[1]) {
                        if (buildHost[0]=='master') {
                            //sh "makeDdeb -vDS -l ${buildNumber}"
                        } else {
                            // Fix CONFIG file for make localinstall target
                            dir(workspace + '/' + '/doocs/' + buildHost[1]) {
                                echo pwd()
                                sh "ls -l CONFIG"
                                sh "sed 's/-I\$(SYSDOOCSINC)//' CONFIG > CONFIG.tmp"
                                sh "mv CONFIG.tmp CONFIG"
                                sh "sed 's/-L\$(SYSDOOCSLIBS)//' CONFIG > CONFIG.tmp"
                                sh "mv CONFIG.tmp CONFIG"
                            }
                            // Copy artifacts using plain methos
                            for (libArtifacts in mapToList(libraryDependencyList)) {
                                def libPath = rootPath + buildHost[1] + '/workspace/' + libArtifacts[0] + '/doocs/' + buildHost[1] + '/lib'
                                echo 'Library path is ' + libPath
                                def targetPath = workspace + '/' + '/doocs/' + buildHost[1]
                                echo 'targetPath is ' + targetPath
                                sh "cp -r ${libPath} ${targetPath}"
                            }
                            // Do the compilation
                            sh "make localinstall"
                        }
                    }
                }
            }
            catch (e) {
                mail(to: mailRecipient[0], 
                        subject: 'Failed ' 
                                + repo[0] 
                            + ' build on ' 
                            + buildHost[0], 
                        body: 'Failed ' 
                                + repo[0] 
                                + ' build on ' 
                            + buildHost[0] 
                            + " ${env.BUILD_URL} "
                            + " ${currentBuild.fullDisplayName}")
            throw e
            } 
//            finally {
//                mail(to: mailRecipient[0], 
//                        subject: 'Successful ' 
//                                + repo[0] 
//                                + ' build on ' 
//                                + buildHost[0], 
//                        body: 'Successful build of ' 
//                            + repo[0] 
//                            + ' on ' 
//                            + buildHost[0] 
//                            + " ${env.BUILD_URL} "
//                            + " ${currentBuild.fullDisplayName}")
//            }

            try {
                stage(buildHost[1] + ' Test ') { 
                    dir(workspace + '/' + repo[1]) {
                        def libPath = rootPath + buildHost[1] + '/workspace/' + repo[0] + '/doocs/' + buildHost[1] + '/lib'
//                        sh "export LD_LIBRARY_PATH=${libPath}; make test"
                    }
                }
            }
            catch (e) {
                 mail(to: mailRecipient[0], 
                            subject: 'Tests failed for ' 
                                    + repo[0] 
                                + ' on ' 
                                + buildHost[0], 
                            body: 'Test failed for ' 
                                    + repo[0] 
                                    + ' on ' 
                                + buildHost[0] 
                                + " ${env.BUILD_URL} "
                                + " ${currentBuild.fullDisplayName}")
                 throw e
           }
//                    step([$class: 'WarningsPublisher', 
//                            canComputeNew: false, 
//                            canResolveRelativePaths: false, 
//                            consoleParsers: [[parserName: 'GNU Make + GNU C Compiler (gcc)'], 
//                                            [parserName: 'Clang (LLVM based)']], 
//                            defaultEncoding: '', 
//                            excludePattern: '', 
//                            healthy: '', 
//                            includePattern: '',
//                            messagesPattern: '', 
//                            unHealthy: ''])
        }
    }
}

@NonCPS
def mapToList(depmap) {
    def dlist = []
    for (entry in depmap) {
        dlist.add([entry.key, entry.value])
    }   
    dlist
}

