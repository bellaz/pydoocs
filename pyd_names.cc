#include "pyd_names.h"
#include <Python.h>
#include "eq_client.h"
#include "pyd_util.h"

// pydoocs names functions provided by the Python bindings
PyObject* pydoocsNames(PyObject* Py_UNUSED(ignored), PyObject* args) {
    PyObject* dataToPython = NULL;  // data from DOOCS get into this PyObject
    const char* doocsAddress;  // address of the DOOCS channel to read from
    
    // Parsing and assigning function arguments: just the doocsAddress here
    if (!PyArg_ParseTuple(args, "s", &doocsAddress)) {
        return NULL;
    }
    
    EqData dataFromDoocs;  // encapsulates data to be read from DOOCS
    int errCode;
    
    EqAdr address;
    EqCall call;
    address.adr(doocsAddress);
    errCode = call.names(&address, &dataFromDoocs);
    if (errCode == 0) {
        int numberOfElements = dataFromDoocs.length();
        if (numberOfElements == 0) Py_RETURN_NONE;
        dataToPython = PyTuple_New(numberOfElements);
        char buffer[80];
        strcpy(buffer, "");
        for (int i = 0; i < numberOfElements; ++i) {
      	     dataFromDoocs.get_string_arg(i, buffer, sizeof(buffer));
             PyTuple_SetItem(dataToPython, i, Py_BuildValue("s", buffer));
        }
        return dataToPython;
    }

    apiSetError(&dataFromDoocs);
    return NULL;
}
