/*
 * pyd_parse.cpp provides functions to parse python objects in C++ types
 *
 *  Author: Andrea Bellandi andrea.bellandi@desy.de
 *
 */ 

#include "pyd_parse.h"

bool parsePyDouble(PyObject* py_obj, double& parsed, std::string& error_str){
   /*
    * Parse a double from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to std::string that describe errors in parsing
    */
    PyObject* double_parsed = NULL;
    error_str = "unable to parse double";
    
    if(!PyNumber_Check(py_obj)) return false;
    double_parsed = PyNumber_Float(py_obj);
    
    if(!double_parsed) return false;
    parsed = PyFloat_AsDouble(double_parsed);
    Py_DECREF(double_parsed);

    error_str = "";
    return true;
} 

bool parsePyLongLong(PyObject* py_obj, long long& parsed, std::string& error_str){
   /*
    * Parse a long long from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to std::string that describe errors in parsing
    */
    PyObject* longlong_parsed = NULL;
    error_str = "unable to parse long long";

    if(!PyNumber_Check(py_obj)) return false;
    longlong_parsed = PyNumber_Long(py_obj);
    
    if(!longlong_parsed) return false;
    parsed = PyLong_AsLongLong(longlong_parsed);
    Py_DECREF(longlong_parsed);

    error_str = "";    
    return true;
}

bool parsePyString(PyObject* py_obj, std::string& parsed, std::string& error_str){
   /*
    * Parse a std::string from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to std::string that describe errors in parsing
    */
    char * string_parsed = NULL;
    error_str = "unable to parse std::string";
    
    if(!PyUnicode_Check(py_obj)) return false;
    string_parsed = PyUnicode_AsUTF8AndSize(py_obj, NULL);    

    if(!string_parsed) return false;
    parsed = string_parsed;

    error_str = "";
    return true;
} 


bool parsePyFloat(PyObject* py_obj, float& parsed, std::string& error_str){
   /*
    * Parse a float from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to float that describe errors in parsing
    */
    
    double double_parsed;

    if (parsePyDouble(py_obj, double_parsed, error_str)){
        
        parsed = static_cast<float>(double_parsed);
        return true;
    } else {
        error_str = "unable to parse float";
        return false;
    }
}

bool parsePyBool(PyObject* py_obj, bool& parsed, std::string& error_str){
   /*
    * Parse a bool from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to short that describe errors in parsing
    */

    long long long_parsed;

    if (parsePyLongLong(py_obj, long_parsed, error_str)){
        
        parsed = static_cast<bool>(long_parsed);
        return true;
    } else {
        error_str = "unable to parse short";
        return false;
    }
}

bool parsePyUChar(PyObject* py_obj, u_char& parsed, std::string& error_str){
   /*
    * Parse a u_char from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to short that describe errors in parsing
    */

    long long long_parsed;

    if (parsePyLongLong(py_obj, long_parsed, error_str)){
        
        parsed = static_cast<u_char>(long_parsed);
        return true;
    } else {
        error_str = "unable to parse u_char";
        return false;
    }
}

bool parsePyShort(PyObject* py_obj, short& parsed, std::string& error_str){
   /*
    * Parse a short from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to short that describe errors in parsing
    */

    long long long_parsed;

    if (parsePyLongLong(py_obj, long_parsed, error_str)){
        
        parsed = static_cast<short>(long_parsed);
        return true;
    } else {
        error_str = "unable to parse short";
        return false;
    }
}

bool parsePyUShort(PyObject* py_obj, u_short& parsed, std::string& error_str){
   /*
    * Parse an u_short from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to short that describe errors in parsing
    */

    long long long_parsed;

    if (parsePyLongLong(py_obj, long_parsed, error_str)){
        
        parsed = static_cast<u_short>(long_parsed);
        return true;
    } else {
        error_str = "unable to parse u_short";
        return false;
    }
}


bool parsePyInt(PyObject* py_obj, int& parsed, std::string& error_str){
   /*
    * Parse a int from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to int that describe errors in parsing
    */
    
    long long long_parsed;

    if (parsePyLongLong(py_obj, long_parsed, error_str)){
        
        parsed = static_cast<int>(long_parsed);
        return true;
    } else {
        error_str = "unable to parse int";
        return false;
    }
}

bool parsePyUInt(PyObject* py_obj, u_int& parsed, std::string& error_str){
   /*
    * Parse an u_int from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to int that describe errors in parsing
    */
    
    long long long_parsed;

    if (parsePyLongLong(py_obj, long_parsed, error_str)){
        
        parsed = static_cast<u_int>(long_parsed);
        return true;
    } else {
        error_str = "unable to parse u_int";
        return false;
    }
}


bool parsePyTimeT(PyObject* py_obj, time_t& parsed, std::string& error_str){
   /*
    * Parse a time_t from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to time_t that describe errors in parsing
    */
 
    long long long_parsed;

    if (parsePyLongLong(py_obj, long_parsed, error_str)){
        
        parsed = static_cast<time_t>(long_parsed);
        return true;
    } else {
        error_str = "unable to parse time_t";
        return false;
    }
}

bool parsePyLong(PyObject* py_obj, long& parsed, std::string& error_str){
   /*
    * Parse a long from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to time_t that describe errors in parsing
    */
 
    long long long_parsed;

    if (parsePyLongLong(py_obj, long_parsed, error_str)){
        
        parsed = static_cast<long>(long_parsed);
        return true;
    } else {
        error_str = "unable to parse long";
        return false;
    }
}

bool parsePyULong(PyObject* py_obj, u_long& parsed, std::string& error_str){
   /*
    * Parse an u_long from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to time_t that describe errors in parsing
    */
 
    long long long_parsed;

    if (parsePyLongLong(py_obj, long_parsed, error_str)){
        
        parsed = static_cast<u_long>(long_parsed);
        return true;
    } else {
        error_str = "unable to parse u_long";
        return false;
    }
}

bool parsePyChar(PyObject* py_obj, char& parsed, std::string& error_str){
   /*
    * Parse a char from pyobject checking passed object. return true if successful,
    * false otherwise
    * py_obj: Python object to parse
    * parsed: pointer to a location where the parsed data has to be stored
    * error: reference to std::string that describe errors in parsing
    */
    
    std::string string_parsed;

    if (parsePyString(py_obj, string_parsed, error_str)){
        
        if (string_parsed.size() != 1) {
            error_str = "passed std::string must be 1 character long";
            return false;
        }

        parsed = string_parsed[0];
        return true;
    } else {

        return false;
    }
}

bool parsePyType(PyObject* py_obj, double& parsed, std::string& error_str){

    return parsePyDouble(py_obj, parsed, error_str);
}

bool parsePyType(PyObject* py_obj, float& parsed, std::string& error_str){

    return parsePyFloat(py_obj, parsed, error_str);
}

bool parsePyType(PyObject* py_obj, long long& parsed, std::string& error_str){

    return parsePyLongLong(py_obj, parsed, error_str);
}

bool parsePyType(PyObject* py_obj, std::string& parsed, std::string& error_str){

    return parsePyString(py_obj, parsed, error_str);
}

bool parsePyType(PyObject* py_obj, bool& parsed, std::string& error_str){

    return parsePyBool(py_obj, parsed, error_str);
}

bool parsePyType(PyObject* py_obj, u_char& parsed, std::string& error_str){

    return parsePyUChar(py_obj, parsed, error_str);
}

bool parsePyType(PyObject* py_obj, short& parsed, std::string& error_str){

    return parsePyShort(py_obj, parsed, error_str);
}

bool parsePyType(PyObject* py_obj, u_short& parsed, std::string& error_str){

    return parsePyUShort(py_obj, parsed, error_str);
}

bool parsePyType(PyObject* py_obj, int& parsed, std::string& error_str){

    return parsePyInt(py_obj, parsed, error_str);
}

bool parsePyType(PyObject* py_obj, u_int& parsed, std::string& error_str){

    return parsePyUInt(py_obj, parsed, error_str);
}

bool parsePyType(PyObject* py_obj, long& parsed, std::string& error_str){

    return parsePyLong(py_obj, parsed, error_str);
}

bool parsePyType(PyObject* py_obj, u_long& parsed, std::string& error_str){

    return parsePyULong(py_obj, parsed, error_str);
}

bool parsePyType(PyObject* py_obj, char& parsed, std::string& error_str){

    return parsePyChar(py_obj, parsed, error_str);
}

