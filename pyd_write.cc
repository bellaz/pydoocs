#include <Python.h>
#include "pyd_write.h"
#include "pyd_parse.h"
#include "pyd_util.h"
#include "eq_client.h"
#include "eq_errors.h"

#define MAX_STRING16_LEN 16
#define MAX_STRING_LEN 80
#define MAX_TEXT_LEN (128*1024)

void build_unsupported_err_str(std::string& error_str,const EqData& eq_data){
    /* This function build an 'unsopported' error string using eq_data type
     *  error_str: the string to be built
     *  eq_data: the unsupported data
     *
     */
    error_str = "unsupported data type ";
    error_str += eq_data.type_string();
    error_str += " on write";
}

void py_prepare_to_error_exit(const std::string& error_str){
    /*
     * Decrement py_obj, clear and set the error string in python to prepare
     * a return with errors.
     *
     */

    PyErr_Clear();
    PyErr_SetString(PyExc_RuntimeError,error_str.c_str());
}

PyObject* pydoocsWrite(PyObject* Py_UNUSED(ignored), PyObject* args) {

    int error_code = 1;
    PyObject* py_obj = NULL;  // data to DOOCS get into this PyObject
    std::string error_str = "this string should not be displayed. " \
                             "Please contact the developer"; // error string
    EqCall eq_call; // equipment function call object
    EqAdr eq_addr; // equipment address object
    EqData eq_src; // equipment data object to be sent to server
    EqData eq_dst; // equipment data object to be received from the server

    const char* doocsAddress;  // address of the DOOCS channel to read from

    if (!PyArg_ParseTuple(args, "sO", &doocsAddress, &py_obj)) {

        error_str = "pydoocs.write need exactly two arguments. First argument" \
                     " is the equipment string. Second argument is the data to" \
                     " write";
        PyErr_Clear();
        PyErr_SetString(PyExc_RuntimeError,error_str.c_str());
        Py_RETURN_NONE;

    }

    Py_BEGIN_ALLOW_THREADS;
    eq_addr.adr(doocsAddress); 
    eq_src.init();
    eq_dst.init();
    error_code = eq_call.get(&eq_addr, &eq_src, &eq_dst); 
    Py_END_ALLOW_THREADS;
    // check errors on get function
    if(error_code){

        error_str = "error on acquiring variable values: ";
        error_str += get_error_string(error_code);
        py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
    }

    // set the type of the data to be sent to the equipment as the one that resides
    // in the server 
    eq_src.set_type(eq_dst.type());

    // Set the data based on the datatype.
    switch(eq_dst.type()) {
        case DATA_NULL:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_BOOL:
            {
                bool data;
                if (!parsePyBool(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(data);
                break;    
            }
        case DATA_INT:
            {
                int data;
                if (!parsePyInt(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(data);
                break;
            }
        case DATA_FLOAT:
            {
                float data;
                if (!parsePyFloat(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(data);
                break;
            }
        case DATA_DOUBLE:
            {
                double data;
                if (!parsePyDouble(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(data);
                break;
            }
        case DATA_STRING16:
            {
                std::string data;
                if (!parsePyString(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                if (data.size() > MAX_STRING16_LEN) {
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(data);
                break;
            }
        case DATA_STRING:
            {
                std::string data;
                if (!parsePyString(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                if (data.size() > MAX_STRING_LEN) {
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(data);
                break;
            }
        case DATA_TEXT:
            {
                std::string data;
                if (!parsePyString(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                if (data.size() > MAX_TEXT_LEN) {
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(data);
                break;
            }
        case DATA_XML:
            {
                std::string data;
                if (!parsePyString(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                error_code = eq_src.set(data);
                break;
            }
        case DATA_TDS:
            {
                std::tuple<time_t, float, u_char> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(std::get<0>(data), std::get<1>(data), std::get<2>(data));
                break;
            }
        case DATA_XY:
            {
                std::tuple<float, float> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(std::get<0>(data), std::get<1>(data));
                break;
            }
        case DATA_IIII:
            {
                std::tuple<int, int, int, int> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(std::get<0>(data), std::get<1>(data),
                        std::get<2>(data), std::get<3>(data));
                break;
            }
        case DATA_IFFF:
            {
                std::tuple<int, float, float, float> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(std::get<0>(data), std::get<1>(data),
                        std::get<2>(data), std::get<3>(data));
                break;
            }
        case DATA_USTR:
            {
                std::tuple<int, float, float, time_t, string> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(std::get<0>(data), std::get<1>(data),
                        std::get<2>(data), std::get<3>(data),
                        std::get<4>(data), 0);
                break;
            }
        case DATA_TTII:
            {
                std::tuple<time_t, time_t, int, int> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(std::get<0>(data), std::get<1>(data),
                        std::get<2>(data), std::get<3>(data));
                break;
            }
        case DATA_XYZS:
            {
                std::tuple<int, float, float, float, std::string> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set(std::get<0>(data), std::get<1>(data),
                        std::get<2>(data), std::get<3>(data),
                        std::get<4>(data), 0);
                break;
            }
        case DATA_SPECTRUM:
            {
                std::vector<float> data;
                SPECTRUM* spectrum = eq_dst.get_spectrum();
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                if (spectrum->d_spect_array.d_spect_array_len != data.size()){
                    error_str = "server and client spectrum lenghts don't match";
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                spectrum->d_spect_array.d_spect_array_val = &data[0];
                error_code = eq_src.set(spectrum);
                break;
            }
        case DATA_GSPECTRUM:
            {
                std::vector<float> data;
                GSPECTRUM* spectrum = eq_dst.get_gspectrum();
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                if (spectrum->d_gspect_array.d_gspect_array_len != data.size()){
                    error_str = "server and client gspectrum lenghts don't match";
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                spectrum->d_gspect_array.d_gspect_array_val = &data[0];
                error_code = eq_src.set(spectrum);
                break;
            }       
        case DATA_IMAGE:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_BOOL:
            {
                std::vector<bool> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                for(unsigned int i = 0; i < data.size(); i++){   
                    error_code = eq_src.set(data[i], (int)i);
                    if(error_code) break;
                }
                break;
            }
        case DATA_A_BYTE:
            {
                std::vector<u_char> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                for(unsigned int i = 0; i < data.size(); i++){
                    error_code = eq_src.set(data[i], (int)i);
                    if (error_code) break;
                }
                break;
            }
        case DATA_A_SHORT:
            {
                std::vector<short> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                for(unsigned int i = 0; i < data.size(); i++){
                    error_code = eq_src.set(data[i], (int)i);
                    if (error_code) break;
                }
                break;
            }
        case DATA_A_INT:
            {
                std::vector<int> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                for(unsigned int i = 0; i < data.size(); i++){
                    error_code = eq_src.set(data[i], (int)i);
                    if (error_code) break;
                }
                break;
            }
        case DATA_A_LONG:
            {
                std::vector<long long> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                for(unsigned int i = 0; i < data.size(); i++){
                    error_code = eq_src.set(data[i], (int)i);
                    if (error_code) break;
                }
                break;
            }
        case DATA_A_FLOAT:
            {
                build_unsupported_err_str(error_str, eq_dst);
                std::vector<float> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                for(unsigned int i = 0; i < data.size(); i++){
                    error_code = eq_src.set(data[i], (int)i);
                    if (error_code) break;
                }
                break;
            }
        case DATA_A_DOUBLE:
            {
                std::vector<double> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                for(unsigned int i = 0; i < data.size(); i++){
                    error_code = eq_src.set(data[i], (int)i);
                    if (error_code) break;
                }
                break;
            }
        case DATA_A_STRING:
            {
                // FIXME: Type present but there isn't any function to set array of 
                // strings
                std::vector<std::string> data;
                std::string data_acc;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                for(unsigned int i = 0; i < data.size(); i++) data_acc += data[i];
                error_code = eq_src.set(data_acc);
                break;
            }
        case DATA_A_XY:
            {
                std::vector<std::tuple<float, float>> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                for(unsigned int i = 0; i < data.size(); i++) {
                    error_code = eq_src.set(std::get<0>(data[i]), std::get<1>(data[i]), i);
                    if (error_code) break;
                }
                break;
            }
        case DATA_A_TDS:
            {
                std::vector<std::tuple<time_t, float, u_char>> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                for(unsigned int i = 0; i < data.size(); i++){
                    error_code = eq_src.set(std::get<0>(data[i]), std::get<1>(data[i]),
                            std::get<2>(data[i]), i);
                }
                if (error_code) break;
                break;
            }
        case DATA_A_USTR:
            {
                std::vector<std::tuple<int, float, float, time_t, std::string>> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                for(unsigned int i = 0; i < data.size(); i++){
                    error_code = eq_src.set(std::get<0>(data[i]),std::get<1>(data[i]),
                            std::get<2>(data[i]), std::get<3>(data[i]),
                            std::get<4>(data[i]), i);
                    if (error_code) break;
                }
                break;
            }
        case DATA_A_XYZS:
            {
                std::vector<std::tuple<int, float, float, float, std::string>> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                for(unsigned int i = 0; i < data.size(); i++){
                    error_code = eq_src.set(std::get<0>(data[i]), std::get<1>(data[i]),
                            std::get<2>(data[i]), std::get<3>(data[i]),
                            std::get<4>(data[i]).c_str(), i);
                    if (error_code) break;
                }
                break;
            }
        case DATA_MDA_FLOAT:
            {
                int dims_mda = eq_dst.get_mda()->dimn;
                int *dims_mda_ptr = eq_dst.get_mda()->dims;

                if (dims_mda != 2) {
                    error_str = "only dim=2 matrix type is supported for DATA_MDA_FLOAT";
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                std::vector<std::vector<float>> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                if (data.size() != (size_t)dims_mda_ptr[0]) {
                    error_str = "server and client row lengths don't match";
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                eq_dst.set_dims(dims_mda_ptr, dims_mda, NULL);

                for(int i = 0; i < dims_mda_ptr[0]; i++){
                    if (data[i].size() != (size_t)dims_mda_ptr[1]) {
                        error_str = "server and client column lengths don't match";
                        py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                    }
                }

                for(int i = 0; i < dims_mda_ptr[0]; i++){
                    for(int j = 0; j < dims_mda_ptr[1]; j++){
                        int n_ind = i * dims_mda_ptr[1] + j;
                        error_code = eq_src.set(data[i][j], n_ind);
                        if (error_code) break;
                    }
                }
                break; 
            }
        case DATA_MDA_DOUBLE:
            {
                int dims_mda = eq_dst.get_mda()->dimn;
                int *dims_mda_ptr = eq_dst.get_mda()->dims;

                if (dims_mda != 2) {
                    error_str = "only dim=2 matrix type is supported for DATA_MDA_DOUBLE";
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                std::vector<std::vector<double>> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                if (data.size() != (size_t)dims_mda_ptr[0]) {
                    error_str = "server and client row lengths don't match";
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }

                eq_dst.set_dims(dims_mda_ptr, dims_mda, NULL);

                for(int i = 0; i < dims_mda_ptr[0]; i++){
                    if (data[i].size() != (size_t)dims_mda_ptr[1]) {
                        error_str = "server and client column lengths don't match";
                        py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                    }
                }

                for(int i = 0; i < dims_mda_ptr[0]; i++){
                    for(int j = 0; j < dims_mda_ptr[1]; j++){
                        int n_ind = i * dims_mda_ptr[1] + j;
                        error_code = eq_dst.set(data[i][j], n_ind);
                        if (error_code) break;
                    }
                }
                break; 
            }

        case DATA_A_THUMBNAIL:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_BOOL:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_INT:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_FLOAT:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_DOUBLE:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_STRING:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_USTR:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_XML:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_XY:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_IIII:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_IFFF:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_SPECTRUM:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_XYZS:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_A_TS_GSPECTRUM:
            build_unsupported_err_str(error_str, eq_dst);
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;

        case DATA_KEYVAL:
            {
                std::tuple<std::string, std::string> data;
                if (!parsePyType(py_obj, data, error_str)){
                    py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
                }
                error_code = eq_src.set_val(std::get<0>(data).c_str(), std::get<1>(data).c_str());
            }
            break;

        default:
            error_str = "unsupported/unknown data type";
            py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
            break;
    }

    // controls if some errors happened setting the variables
    if (!error_code){ //From the docs it seems that error_code = 1 is ok...
        error_str = "error on setting variables: ";
        error_str += get_error_string(error_code);
        py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
    }

    Py_BEGIN_ALLOW_THREADS;
    eq_call.set(&eq_addr, &eq_src, &eq_dst);
    error_code = eq_dst.error();
    Py_END_ALLOW_THREADS;

    if(error_code){
        error_str = "error on sending data: ";
        error_str += get_error_string(error_code);
        py_prepare_to_error_exit(error_str);  Py_RETURN_NONE;
    }

    Py_RETURN_NONE;  // return Python's NoneType
}
